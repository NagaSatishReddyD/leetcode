
public class DefanginindIpAddress {

	public static void main(String[] args) {
		DefanginindIpAddress ad = new DefanginindIpAddress();
		System.out.println(ad.defangIPaddr("255.100.50.0"));
	}
	
	public String defangIPaddr(String address) {
        return address.replace(".", "[.]");
    }
}
