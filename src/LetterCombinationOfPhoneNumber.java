import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LetterCombinationOfPhoneNumber {

	public static void main(String[] args) {
		System.out.println(letterCombinations("23"));
		
//		System.out.println(Pattern.compile("A.*D").matcher("ABCD").matches());
//		int i = 0;
//		String a = "ABCDEFGHIJKLMN";
//		String symbol = ".*";
//		while(i <= a.length()) {
//			String regexString = a.substring(0, i)+symbol+a.substring(i);
//			System.out.println(regexString);
//			i++;
//		}
	}
	
	public static List<String> letterCombinations(String digits) {
		Map<String, String> numberMap = getPhoneNumberMap();
		List<String> letterCombinationMap = new ArrayList<>();
		if(digits.length() == 1) {
			letterCombinationMap = Stream.of(numberMap.get(digits).split("")).collect(Collectors.toList());
		}else {
			String lastLetter = digits.substring(digits.length()-1);
			letterCombinationMap = letterCombinations(digits.substring(0, digits.length() -1));
			List<String> lastDigitData = Stream.of(numberMap.get(lastLetter).split("")).collect(Collectors.toList());
			for(int i = 0; i < letterCombinationMap.size(); i++) {
				for(int j = 0; j < lastDigitData.size(); j++) {
					String value = letterCombinationMap.get(i)+lastDigitData.get(j);
					letterCombinationMap.set(i, value);
				}
			}
		}
		return letterCombinationMap;
    }

	private static Map<String, String> getPhoneNumberMap() {
		// TODO Auto-generated method stub
		Map<String, String> numberMap = new HashMap<>();
		numberMap.put("2", "abc");
		numberMap.put("3", "def");
		numberMap.put("4", "ghi");
		numberMap.put("5", "jkl");
		numberMap.put("6", "mno");
		numberMap.put("7", "pqrs");
		numberMap.put("8", "tuv");
		numberMap.put("9", "wxyz");
		return numberMap;
	}

}
