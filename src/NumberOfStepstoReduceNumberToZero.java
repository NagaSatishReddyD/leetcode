
public class NumberOfStepstoReduceNumberToZero {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		NumberOfStepstoReduceNumberToZero numberToReduce = new NumberOfStepstoReduceNumberToZero();
		System.out.println(numberToReduce.numberOfSteps(123));
	}

	public int numberOfSteps (int num) {
        int count = 0;
        while(num != 0) {
        	if(num % 2 == 0) {
        		num /= 2;
        	}else {
        		num--;
        	}
        	count++;
        }
        return count;
    }
}
