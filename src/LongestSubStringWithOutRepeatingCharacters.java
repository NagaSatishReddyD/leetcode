
public class LongestSubStringWithOutRepeatingCharacters {

	public static void main(String[] args) {
		System.out.println(lengthOfLongestSubstring("aab"));
	}

	public static int lengthOfLongestSubstring(String s) {
		int max = 0;
		String requiredString = "";
		int i = 0;
		while(i < s.length()) {
			String value = s.substring(i, i+1);
			if(requiredString.contains(value)) {
				requiredString = requiredString.substring(requiredString.indexOf(value)+1);
				requiredString += s.substring(i, i+1);
			}else {
				requiredString += s.substring(i, i+1);
				max = max < requiredString.length() ? requiredString.length() : max;
			}
			i++;
		}
		return max;
	}
}
