
public class MedianOfTwoSortedArrays {

	public static void main(String[] args) {
		int [] nums1 = {1, 3};
		int [] nums2 = {2};
		System.out.println(findMedianSortedArrays(nums1, nums2));
	}
	
	public static double findMedianSortedArrays(int[] nums1, int[] nums2) {
        int totalLength = nums1.length + nums2.length;
        int medianPostion = totalLength / 2;
        boolean isOdd = totalLength % 2 == 0 ? false:true;
        int firstMediam = Integer.MIN_VALUE;
        int secondMediam = Integer.MIN_VALUE;
        int firstStart = 0;
        int secondStart = 0;
        int position = 0;
        while(position <= medianPostion && firstStart < nums1.length && secondStart < nums2.length) {
        	position++;
    		secondMediam = firstMediam;
        	if(nums1[firstStart] < nums2[secondStart]) {
        		firstMediam = nums1[firstStart++];
        	}else {
        		firstMediam = nums2[secondStart++];
        	}
        }
        
        while(position <= medianPostion && firstStart < nums1.length) {
        	position++;
        	secondMediam = firstMediam;
        	firstMediam = nums1[firstStart++];
        }
        
        while(position <= medianPostion && secondStart < nums2.length) {
        	position++;
        	secondMediam = firstMediam;
        	firstMediam = nums2[secondStart++];
        }
        
        return isOdd ? firstMediam: (secondMediam+firstMediam)/2.0;
        
    }

}
