import java.util.ArrayList;
import java.util.List;

public class DecompressRunLengthEncodedList {

	public static void main(String[] args) {
		int [] nums = {1,2,3,4};
		DecompressRunLengthEncodedList dec = new DecompressRunLengthEncodedList();
		int[] numbersArray = dec.decompressRLElist(nums);
		for(int i =0; i < numbersArray.length;i++) {
			System.out.println(numbersArray[i]);
		}
	}
	
	public int[] decompressRLElist(int[] nums) {
		List<Integer> numbersList = new ArrayList<Integer>();
        for(int i = 0; i <= nums.length-2; i+=2) {
        	for(int j = 0; j < nums[i]; j++) {
        		numbersList.add(nums[i+1]);
        	}
        }
        int [] numberArray = new int[numbersList.size()];
        for(int i = 0; i < numbersList.size(); i++) {
        	numberArray[i] = numbersList.get(i);
        }
        return numberArray;
    }

}
