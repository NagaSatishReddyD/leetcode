package com.leetcode.easy;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class TwoSum {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int [] numbers = new int[Integer.parseInt(scan.nextLine())];
		for(int i = 0; i < numbers.length; i++) {
			numbers[i] = Integer.parseInt(scan.nextLine());
		}
		int target = Integer.parseInt(scan.nextLine());
		int[] returnedArray = twoSum(numbers, target);
		System.out.println(returnedArray[0] +" "+returnedArray[1]);
		scan.close();
	}
	
	public static int[] twoSum(int[] nums, int target) {
		//HashMap Single Look Ups
		Map<Integer, Integer> numbersMap = new HashMap<>();
		for(int i = 0; i < nums.length; i++) {
			int complement = target - nums[i];
			if(numbersMap.containsKey(complement)) {
				return new int[] {i, numbersMap.get(complement)};
			}
			numbersMap.put(nums[i], i);
		}
		return new int[] {};
		
		
		//Hash Map Two Look Ups
		/*
		Map<Integer, Integer> numbersMap = new HashMap<>();
		for(int i = 0; i < nums.length; i++) {
			numbersMap.put(nums[i], i);
		}
		
		for(int i = 0; i < nums.length; i++) {
			int complement = target - nums[i];
			if(numbersMap.containsKey(complement) && numbersMap.get(complement) != i) {
				return new int [] {i, numbersMap.get(complement)};
			}
		}
		return new int [] {};
		*/
		
		//Brute Force
		/*
		int [] requiredArray = new int[2];
		for(int i = 0; i < nums.length; i++) {
			for(int j = i+1; j < nums.length;j++){
				if(nums[i] + nums[j] == target) {
					requiredArray[0] = i;
					requiredArray[1] = j;
					return requiredArray;
				}
			}
		}
		
		return requiredArray; */
    }

}
