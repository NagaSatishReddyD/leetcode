package com.leetcode.easy;

public class SymmetricTree {

	public static void main(String[] args) {
		TreeNode t1 = new TreeNode(1, new TreeNode(2, new TreeNode(3), new TreeNode(4)), new TreeNode(2, new TreeNode(4), new TreeNode(3)));
		System.out.println(isSymmetric(t1));;
	}
	


	public static boolean isSymmetric(TreeNode root) {
		if(root == null) {
			return true;
		}
		return symmetricTreeDecision(root.left, root.right);
	}


	private static boolean symmetricTreeDecision(TreeNode p, TreeNode q) {
		if(p == null && q == null) {
			return true;
		}else if((p == null && q != null) || (p != null && q == null) || (p.val != q.val)) {
			return false;
		}
		return symmetricTreeDecision(p.left, q.right) && symmetricTreeDecision(p.right, q.left);
	}


}
