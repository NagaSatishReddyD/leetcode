package com.leetcode.easy;

public class MergeSortedArray {

	public static void main(String[] args) {
		int [] arrayOne = {0};
		int [] arrayTwo = {1};
		int m = 0;
		int n = 1;
		int[] nums1 = merge(arrayOne, m, arrayTwo, n);
		for(int i = 0; i < nums1.length;i++)
			System.out.println(nums1[i]);
	}

	public static int [] merge(int[] nums1, int m, int[] nums2, int n) {
        int end = m+n-1;
        while(m-1 >= 0 && n-1 >= 0) {
        	if(nums1[m] >= nums2[n]) {
        		nums1[end--] = nums1[--m]; 
        	}else {
        		nums1[end--] = nums2[--n];
        	}
        }
        
        while(m-1>=0) {
        	nums1[end--] = nums1[--m];
        }
        while(n-1>=0) {
        	nums1[end--] = nums2[--n];
        }
        return nums1;
    }
}
