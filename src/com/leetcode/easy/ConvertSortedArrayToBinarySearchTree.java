package com.leetcode.easy;

public class ConvertSortedArrayToBinarySearchTree {

	public static void main(String[] args) {
		int []sortedArray = {-10,-3,0,5,9};
		System.out.println(sortedArrayToBST(sortedArray));
	}

	public static TreeNode sortedArrayToBST(int[] nums) {
		if(nums.length == 0) {
			return null;
		}
		TreeNode root = null;
		int low = 0;
		int high = nums.length-1;
		//		return binaryIteration(nums, low, high, root);
		return convertSortedToBinary(nums, low, high);
	}
	private static TreeNode convertSortedToBinary(int[] nums, int low, int high) {
		if(low > high) {
			return null;
		}
		int mid = (low+high)/2;
		TreeNode left = convertSortedToBinary(nums, low, mid-1);
		TreeNode right = convertSortedToBinary(nums, mid+1, high);
		return new TreeNode(nums[mid], left, right);
	}

	private static TreeNode binaryIteration(int []nums, int low, int high, TreeNode root) {
		int mid = (low + high) / 2;
		if(low<=high) {
			root = insertNode(root, nums[mid]);
			binaryIteration(nums, low, mid-1, root);
			binaryIteration(nums, mid+1, high, root);
		}
		return root;
	}

	private static TreeNode insertNode(TreeNode root, int val) {
		if(root == null) {
			System.out.println("created "+ val);
			return new TreeNode(val);
		}
		if(root.val < val) {
			root.right = insertNode(root.right, val);
		}else {
			root.left = insertNode(root.left, val);
		}
		return root;
	}
}
