package com.leetcode.easy;

import java.util.ArrayList;
import java.util.List;

public class PascalTriangleII {

	public static void main(String[] args) {
		List<Integer> result = getRow(4);
		for(int i = 0; i < result.size(); i++) {
			System.out.print(result.get(i)+" ");
		}

	}

	public static List<Integer> getRow(int rowIndex) {
		List<Integer> pascalRow = new ArrayList<>();
		pascalRow.add(1);
		if(rowIndex != 0) {
			while(rowIndex >= 1) {
				for(int j = 1; j < pascalRow.size();j++) {
					pascalRow.set(j-1, pascalRow.get(j-1)+pascalRow.get(j));
				}
				rowIndex--;
				pascalRow.add(0,1);
			}
		}
		return pascalRow;
	}
}
