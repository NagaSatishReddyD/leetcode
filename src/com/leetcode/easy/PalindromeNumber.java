package com.leetcode.easy;

public class PalindromeNumber {

	public static void main(String[] args) {
		PalindromeNumber pn = new PalindromeNumber();
		System.out.println(pn.isPalindrome(0));
	}
	
	public boolean isPalindrome(int x) {
		//Reverse only half a number
		if(x < 0 || (x%10 == 0 && x != 0)) {
			return false;
		}else {
			int result = 0;
			while(result <= x) {
				int rem = x % 10;
				result = result * 10 + rem;
				x /= 10;
			}
			return x == result || x == result/10;
		}
		
		//Reversing complete number
        /*if(x < 0) {
        	return false;
        }else {
        	int result = 0;
        	int n = x;
        	while(x != 0) {
        		int rem = x % 10;
        		result = result * 10 + rem;
        		x /= 10;
        	}
        	return n == result ? true: false;
        }*/
    }

}
