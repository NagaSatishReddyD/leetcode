package com.leetcode.easy;

public class CountAndSay {

	public static void main(String[] args) {
		System.out.println(countAndSay(3));
	}

	private static String countAndSay(int numberIndex) {
		String number  = "1";
		while(numberIndex != 1) {
			String nextNumber = "";
			int i = 0;
			int count = 1;
			while(i+1 <= number.length()) {
				if(i+1 == number.length() || number.charAt(i) != number.charAt(i+1)) {
					nextNumber += count+number.substring(i, i+1);
					count = 1;
				}else {
					count++;
				}
				i++;
			}
			number = nextNumber;
			numberIndex--;
		}
		return number;
	}
}
