package com.leetcode.easy;

public class MaximumSubArray {

	public static void main(String[] args) {
		int nums [] = {-2,-1,-3,-4,-1,-2,-1,-5,-4};
		System.out.println(maxSubArray(nums));
	}

	public static int maxSubArray(int[] nums) {
		/*if(nums.length == 0) {
			return 0;
		}
		int maxSum = nums[0];
		int sum = nums[0];
		for(int i = 1; i < nums.length; i++) {
			sum += nums[i];
			if(sum >= maxSum) {
				maxSum = sum;
			}
			if(sum < 0){
				sum = 0;
			}
		}
		return maxSum; */
		//divide and conquer need to be done
		return findTheMaxSubArraySum(nums, 0, nums.length);
	}

	private static int findTheMaxSubArraySum(int[] nums, int low, int high) {
		if(low == high) {
			return nums[low];
		}
		int mid = (low+high) / 2;
		int lsum = findTheMaxSubArraySum(nums, low, mid);
		int rsum = findTheMaxSubArraySum(nums, mid+1, high);
		if(lsum >= rsum) {
			return lsum;
		}else {
			return rsum;
		}
	}
}
