package com.leetcode.easy;

public class MaximumDepthOfBinaryTree {

	public static void main(String[] args) {
		TreeNode t1 = new TreeNode(3, new TreeNode(9), new TreeNode(20, new TreeNode(15), new TreeNode(7)));
		System.out.println(maxDepth(t1));
	}
	
	public static int maxDepth(TreeNode root) {
		if(root == null) {
			return 0;
		}
		int leftHeight = maxDepth(root.left);
		int rightHeight = maxDepth(root.right);
		if(leftHeight > rightHeight) {
			return 1+leftHeight;
		}else {
			return 1+rightHeight;
		}
    }

}
