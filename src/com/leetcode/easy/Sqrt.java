package com.leetcode.easy;

public class Sqrt {

	public static void main(String[] args) {
		for(int i = 1; i < 100; i++)
			System.out.println(i+" -- "+mySqrt(i));
	}

	public static int mySqrt(int x) {
		if(x == 0) {
			return 0;
		}
		if(x == 1) {
			return 1;
		}
		int low = 1;
		int high = x / 2 ;
		do {
			int mid = (low+high)/2;
			if(mid <= x/mid && (mid+1) > x / (mid+1)) {
				return mid;
			}else if(mid < x/mid) {
				low = mid+1;
			}else {
				high = mid-1;
			}
		}while(low<=high);
		return 0;
	}

}
