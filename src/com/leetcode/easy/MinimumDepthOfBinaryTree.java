package com.leetcode.easy;

import java.util.ArrayList;

public class MinimumDepthOfBinaryTree {

	public static void main(String[] args) {
		TreeNode t1 = new TreeNode(3, new TreeNode(9), new TreeNode(20, new TreeNode(15), new TreeNode(7)));
//		TreeNode t1 = new TreeNode(1, new TreeNode(2), null);
		System.out.println(minDepth(t1));
	}
	
	public static int minDepth(TreeNode root) {
		return findLevelOrder(root, 1);
    }

	private static int findLevelOrder(TreeNode root, int level) {
		if(root == null) {
			return 0;
		}
		if(root.left == null && root.right == null) {
			return level;
		}
		int left = findLevelOrder(root.left, level+1);
		int right = findLevelOrder(root.right, level+1);
		if(Math.min(left, right) == 0) {
			return Math.max(left, right);
		}
		return Math.min(left, right);
	}
	
	

}
