package com.leetcode.easy;

public class PlusOne {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(plusOne(new int[] {}));
	}
	
	public static int[] plusOne(int[] digits) {
		int extraAdder = 1;
       for(int i = digits.length - 1; i >= 0; i--) {
    	   int result = digits[i] + extraAdder;
    	   if(result == 10) {
    		   digits[i] = 0;
    		   extraAdder = 1;
    	   }else {
    		   digits[i] = result;
    		   extraAdder = 0;
    	   }
       }
       if(extraAdder == 1) {
    	   int [] newDigitArray = new int[digits.length+1];
    	   newDigitArray[0] = extraAdder;
    	   for(int i = 0; i < digits.length;i++) {
    		   newDigitArray[i+1] = digits[i];
    	   }
    	   return newDigitArray;
       }
       return digits;
    }

}
