package com.leetcode.easy;

public class BalancedBinaryTree {

	static boolean result = true;
	public static void main(String[] args) {
//		TreeNode t1 = new TreeNode(3, new TreeNode(9), new TreeNode(20, new TreeNode(15), new TreeNode(7)));
//		TreeNode t1 = new TreeNode(1,new TreeNode(2, new TreeNode(3, new TreeNode(4), new TreeNode(4)), new TreeNode(3)),new TreeNode(2));
		
		System.out.println(isBalanced(null));
	}
	public static boolean isBalanced(TreeNode root) {
		int max = maxDepth(root, 0);
		return result;
	}
	private static int maxDepth(TreeNode root, int maxDiff) {
		if(root == null) {
			return 0;
		}
		int leftHeight = 1 + maxDepth(root.left, maxDiff);
		int rightHeight = 1 + maxDepth(root.right, maxDiff);
		if(Math.abs(leftHeight-rightHeight) > 1) {
			result = false;
		}
		return leftHeight > rightHeight ? leftHeight : rightHeight;
	}
}

