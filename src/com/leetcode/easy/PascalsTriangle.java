package com.leetcode.easy;

import java.util.ArrayList;
import java.util.List;

public class PascalsTriangle {

	public static void main(String[] args) {
		List<List<Integer>> result = generate(5);
		for(int i = 0; i < result.size(); i++) {
			for(int j = 0; j < result.get(i).size(); j++) {
				System.out.print(result.get(i).get(j)+" ");
			}
			System.out.println();
		}

	}

	public static List<List<Integer>> generate(int numRows) {
		List<List<Integer>> pascaslTriangleList = new ArrayList<List<Integer>>();
		int i = 0;
		while(i < numRows) {
			pascaslTriangleList.add(createPascalRow(pascaslTriangleList, i));
			i++;
		}
		return pascaslTriangleList;
	}

	private static List<Integer> createPascalRow(List<List<Integer>> pascaslTriangleList, int i) {
		List<Integer> pascalRow = new ArrayList<>();
		if(i == 0) {
			pascalRow.add(1);
		}else {
			pascalRow.add(1);
			pascalRow.add(1);
			List<Integer> previousRow = pascaslTriangleList.get(i - 1);
			int j = 1;
			int rowIndex = 1;
			while(j < previousRow.size()) {
				int sum = previousRow.get(j-1)+previousRow.get(j);
				pascalRow.add(rowIndex++, sum);
				j++;
			}
		}
		return pascalRow;
	}
}
