package com.leetcode.easy;

import java.util.Stack;

public class ValidParanthesis {

	public static void main(String[] args) {
		String s = "]";
		Stack<Character> characterStack = new Stack<>();
		int i = 0;
		while(i < s.length()) {
			char currentCharacter = s.charAt(i); 
			if(currentCharacter == '(' || currentCharacter == '{' || currentCharacter == '[') {
				characterStack.push(currentCharacter);
			}else {
				if(characterStack.size() <= 0)
					System.out.println(false);
				Character previousCharacter = characterStack.pop();
				if(!(currentCharacter == ')' && previousCharacter == '(' || currentCharacter == '}' && previousCharacter == '{' || currentCharacter == ']' && previousCharacter == '[' ))
					System.out.println(false);
			}
			i++;
		}
		System.out.println(characterStack.size() > 0 ? false:true);
	}

}
