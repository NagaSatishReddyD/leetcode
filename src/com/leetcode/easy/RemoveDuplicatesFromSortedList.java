package com.leetcode.easy;

public class RemoveDuplicatesFromSortedList {

	public static void main(String[] args) {
		ListNode l2 = new ListNode(1, new ListNode(1, new ListNode(2, new ListNode(3, new ListNode(3)))));
		ListNode r = deleteDuplicates(l2);
		while (r != null) {
			System.out.println(r.val);
			r=r.next;
		}
	}

	public static ListNode deleteDuplicates(ListNode head) {
		if(head == null || head.next == null) {
			return head;
		}
		if(head.val < head.next.val) {
			head.next = deleteDuplicates(head.next);
			return head;
		}else if(head.val == head.next.val) {
			head = deleteDuplicates(head.next);
			return head;
		}
		return head;
	}

}
