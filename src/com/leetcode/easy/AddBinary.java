package com.leetcode.easy;

public class AddBinary {

	public static void main(String[] args) {
		//System.out.println(addBinary("1010","1011"));
		System.out.println(addBinaryMethod2("1010","1011"));
		//System.out.println(addBinaryMethod2("11","1"));
	}
	
	private static String addBinaryMethod2(String a, String b) {
		int i = a.length()-1;
		int j = b.length()-1;
		String result = "";
		int sum = 0;
		while(i >= 0 || j >= 0) {
			if(i >= 0) {
				sum += a.charAt(i) - '0';
			}
			if(j>=0) {
				sum+= b.charAt(j) - '0';
			}
			
			result = (sum % 2)+result;
			sum/=2;
			i--;
			j--;
		}
		if(sum != 0) {
			result = sum+result;
		}
		return result;
	}

	public static String addBinary(String a, String b) {
        String result = "";
        String carry = "0";
        int i = a.length()-1;
        int j = b.length()-1;
        while(i >= 0 && j >= 0) {
        	String val = additionResult(carry, additionResult(a.substring(i, i+1), b.substring(j, j+1)));
        	if(val.length() == 2) {
        		carry = val.substring(0,1);
        		result = val.substring(1)+result;
        	}else {
        		result = val.substring(0)+result;
        		carry = "0";
        	}
        	i--;
        	j--;
        }
        
        while(i >= 0) {
        	String val = additionResult(carry,a.substring(i, i+1));
        	if(val.length() == 2) {
        		carry = val.substring(0,1);
        		result = val.substring(1)+result;
        	}else {
        		result = val.substring(0)+result;
        		carry = "0";
        	}
        	i--;
        }
        
        while(j >= 0) {
        	String val = additionResult(carry,b.substring(j, j+1));
        	if(val.length() == 2) {
        		carry = val.substring(0,1);
        		result = val.substring(1)+result;
        	}else {
        		result = val.substring(0)+result;
        		carry = "0";
        	}
        	j--;
        }
        if(carry != "0") {
        	result = carry+result;
        }
        return result;   
    }

	public static String additionResult(String a, String b) {
		if((a.equals("1") && b.equals("0")) || (a.equals("0") && b.equals("1"))) {
			return "1";
		}else if((a.equals("1") && b.equals("1")) || (a.equals("10") && b.equals("0")) ||(a.equals("0") && b.equals("10"))) {
			return "10";
		}else if(a.equals("0") && b.equals("0")) {
			return "0";
		}else {
			return "11";
		}
	}
}
