package com.leetcode.easy;

import java.util.Random;

public class MergeTwoSortedLinkedLists {


	public static void main(String[] args) {
		Random rand = new Random();
		System.out.println(rand.nextInt(3));
		ListNode l1 = new ListNode(1, new ListNode(2, new ListNode(4)));
		ListNode l2 = new ListNode(1, new ListNode(3, new ListNode(4, new ListNode(5, new ListNode(6, new ListNode(7))))));
//		ListNode l1 = new ListNode(5);
//		ListNode l2 = new ListNode(1, new ListNode(2, new ListNode(4)));
		MergeTwoSortedLinkedLists lists = new MergeTwoSortedLinkedLists();
		ListNode result = lists.mergeTwoLists(l1, l2);
		while(result != null) {
			System.out.println(result.val);
			result = result.next;
		}
	}

	private ListNode mergeTwoLists(ListNode l1, ListNode l2) {
		if(l1 == null) {
			return l2;
		}
		if(l2 == null) {
			return l1;
		}
		
		if(l1.val <= l2.val) {
			l1.next = mergeTwoLists(l1.next, l2);
			return l1;
		}else {
			l2.next = mergeTwoLists(l1, l2.next);
			return l2;
		}
	}

}

class ListNode {
	int val;
	ListNode next;
	ListNode() {}
	ListNode(int val) { this.val = val; }
	ListNode(int val, ListNode next) { this.val = val; this.next = next; }
}