package com.leetcode.easy;

import java.util.ArrayList;
import java.util.List;

public class BinaryTreeLevelOrderTraversalII {

	public static void main(String[] args) {
		TreeNode t1 = new TreeNode(3, new TreeNode(9), new TreeNode(20, new TreeNode(15), new TreeNode(7)));
		List<List<Integer>> bottomLevel = levelOrderBottom(t1);
		for(int i = 0; i < bottomLevel.size(); i++) {
			for(int j = 0; j < bottomLevel.get(i).size(); j++) {
				System.out.print( bottomLevel.get(i).get(j)+ " ");
			}
			System.out.println();
		}
	}
	
	 public static List<List<Integer>> levelOrderBottom(TreeNode root) {
	        List<List<Integer>> bottomLevelList = new ArrayList<List<Integer>>();
	        findTheLevelOrderValues(root,0, bottomLevelList);
	        return bottomLevelList;
	    }

	private static void findTheLevelOrderValues(TreeNode root,int level, List<List<Integer>> bottomLevelList) {
		if(root == null) {
			return;
		}
		if(bottomLevelList.size() == level) {
			bottomLevelList.add(0, new ArrayList<>());
		}
		bottomLevelList.get(bottomLevelList.size()-1 - level).add(root.val);
		findTheLevelOrderValues(root.left, level+1, bottomLevelList);
		findTheLevelOrderValues(root.right, level+1, bottomLevelList);
	}

}
