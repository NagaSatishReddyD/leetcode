package com.leetcode.easy;

public class ClimbingStairs {

	public static void main(String[] args) {
		System.out.println(climbStairs(7));
	}

	public static int climbStairs(int n) {
		//dynamic programming

		int[] val = new int[n+1];
		val[0] = 1;
		val[1] = 1;
		for(int i = 2; i <= n; i++) {
			val[i] = val[i-1]+val[i-2];
		}
		return val[n];
	
		
		//recursion
//		if(n <= 1) {
//			return n;
//		}
//		return climbStairs(n-1)+climbStairs(n-2);
	}
}
