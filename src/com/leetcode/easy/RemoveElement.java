package com.leetcode.easy;

public class RemoveElement {

	public static void main(String[] args) {
		int [] nums = {0,1,2,2,3,0,4,2};
		RemoveElement rem = new RemoveElement();
		System.out.println(rem.removeElement(nums, 2));
		
	}
	
	public int removeElement(int[] nums, int val) {

		if(nums.length == 0) {
			return 0;
		}
		int index = 0;
		for(int i = 0; i < nums.length; i++) {
			if(nums[i] != val) {
				nums[index++] = nums[i];
			}
		}
		return index;
	
    }

}
