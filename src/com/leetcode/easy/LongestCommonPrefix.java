package com.leetcode.easy;

public class LongestCommonPrefix {

	public static void main(String[] args) {
		String [] strs = {"dog","racecar","car"};
		LongestCommonPrefix prefix = new LongestCommonPrefix();
		System.out.println(prefix.longestCommonPrefix(strs));
	}
	
	public String longestCommonPrefix(String[] strs) {
		if(strs.length == 0) {
			return "";
		}
        String commonPrefix = strs[0];
        for(int i = 1; i < strs.length; i++) {
        	commonPrefix = getCommonWord(commonPrefix, strs[i]);
        }
        return commonPrefix;
    }

	private String getCommonWord(String stringOne, String stringTwo) {
		String commonPrefix = "";
		int  i =0;
		int j = 0;
		while(i < stringOne.length() && j < stringTwo.length() && stringOne.charAt(i) == stringTwo.charAt(j)) {
			commonPrefix += String.valueOf(stringOne.charAt(i));
			i++;
			j++;
		}
		return commonPrefix;
	}

}
