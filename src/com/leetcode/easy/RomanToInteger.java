package com.leetcode.easy;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class RomanToInteger {

	public static void main(String[] args) {
		Map<Character, Integer> romanMap = new HashMap<>();
		romanMap.put('I', 1);
		romanMap.put('V', 5);
		romanMap.put('X', 10);
		romanMap.put('L', 50);
		romanMap.put('C', 100);
		romanMap.put('D', 500);
		romanMap.put('M', 1000);
		
		Scanner scan = new Scanner(System.in);
		String givenRomanNumber = scan.nextLine();
		int i = 0;
		int result = 0;
		while(i < givenRomanNumber.length()) {
			Integer currentValue = romanMap.get(givenRomanNumber.charAt(i));
			if(i == givenRomanNumber.length()-1) {
				result += currentValue;
				i++;
			}else {
				Integer rightNeighbour = romanMap.get(givenRomanNumber.charAt(i+1));
				if(currentValue >= rightNeighbour) {
					result += currentValue;
					i++;
				}else {
					result += (rightNeighbour - currentValue);
					i+=2;
				}
			}
		}
		System.out.println("Result : "+result);
		scan.close();
	}

}
