package com.leetcode.easy;

public class LengthOfLastWord {

	public static void main(String[] args) {
		System.out.println(lengthOfLastWord("Hello World"));
	}

	 public static int lengthOfLastWord(String s) {
	        String[] splitData = s.split(" ");
	        if(splitData.length == 0) {
	        	return 0;
	        }
	        return splitData[splitData.length - 1].length();
	   }
}
