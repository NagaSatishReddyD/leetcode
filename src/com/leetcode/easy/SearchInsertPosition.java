package com.leetcode.easy;

public class SearchInsertPosition {

	public static void main(String[] args) {
		SearchInsertPosition sip = new SearchInsertPosition();
		int [] nums = {1,3,5,6};
		for(int i = 0; i < 8; i++)
			System.out.println(i+" -- "+sip.searchInsert(nums, i));
	}
	
	public int searchInsert(int[] nums, int target) {
        int low = 0;
        int high = nums.length;
        int mid = 0;
        if(target < nums[0]) {
        	return 0;
        }
        if(target > nums[nums.length - 1]) {
        	return nums.length;
        }
		while(low <= high) {
        	mid  = (low+high) / 2;
        	if((nums[mid] == target)) {
        		return mid;
        	}
        	if(target > nums[mid]) {
        		low = mid+1;
        	}	
        	if(target < nums[mid]) {
        		high = mid - 1;
        	}
        }
        return nums[mid] > target? mid: mid+1;
    }

}
