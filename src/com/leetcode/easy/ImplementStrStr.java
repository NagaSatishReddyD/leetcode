package com.leetcode.easy;

public class ImplementStrStr {

	public static void main(String[] args) {
		ImplementStrStr s = new ImplementStrStr();
		System.out.println(s.strStr("a", "a"));
	}
	
	 public int strStr(String haystack, String needle) {
		 if(needle.isEmpty()) {
			 return 0;
		 }else if(haystack.length() < needle.length()) {
			 return -1;
		 }
		 for(int i = 0; i <= haystack.length() - needle.length(); i++) {
			 if(haystack.substring(i, i+needle.length()).equals(needle)) {
				 return i;
			 }
		 }
		 return -1;
	 }

}
