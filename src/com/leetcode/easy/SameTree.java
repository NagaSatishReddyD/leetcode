package com.leetcode.easy;

public class SameTree {
	public static void main(String[] args) {
//		TreeNode t1 = new TreeNode(1, new TreeNode(2), new TreeNode(3));
//		TreeNode t2 = new TreeNode(1, new TreeNode(2), new TreeNode(3));
		TreeNode t1 = new TreeNode(1, new TreeNode(2), null);
		TreeNode t2 = new TreeNode(1, null, new TreeNode(2));
		System.out.println(isSameTree(t1, t2));
	}

	public static boolean isSameTree(TreeNode p, TreeNode q) {
		if(p == null && q == null) {
			return true;
		}else if((p == null && q != null) || (p != null && q == null) || (p.val != q.val)) {
			return false;
		}
		return isSameTree(p.right, q.right) && isSameTree(p.left, q.left);
	}
}


class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;
	TreeNode() {}
	TreeNode(int val) { this.val = val; }
	TreeNode(int val, TreeNode left, TreeNode right) {
		this.val = val;
		this.left = left;
		this.right = right;
	}
}

