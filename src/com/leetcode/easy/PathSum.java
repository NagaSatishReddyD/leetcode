package com.leetcode.easy;

public class PathSum {

	public static void main(String[] args) {
		TreeNode t1 = new TreeNode(5, new TreeNode(4, new TreeNode(11, new TreeNode(7), new TreeNode(2)), null), new TreeNode(8, new TreeNode(13), new TreeNode(4, null, new TreeNode(1))));
		int sum = 22;
		System.out.println(hasPathSum(t1, sum));
	}
	
	public static boolean hasPathSum(TreeNode root, int sum) {
		if(root == null) {
			return false;
		}
		if(root.left == null && root.right == null) {
			return sum - root.val == 0 ? true:false;
		}
		return hasPathSum(root.left, sum-root.val) || hasPathSum(root.right, sum-root.val);
    }
}
