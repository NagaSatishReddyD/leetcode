package com.leetcode.easy;

public class RemoveDuplicatesFromSortedArray {

	public static void main(String[] args) {
		int [] numberArray = {0,0,1,1,1,2,2,3,3,4};
		RemoveDuplicatesFromSortedArray dup = new RemoveDuplicatesFromSortedArray();
		System.out.println(dup.removeDuplicates(numberArray));
	}

	public int removeDuplicates(int[] nums) {
		if(nums.length == 0) {
			return 0;
		}
		int index = 1;
		for(int i = 1; i < nums.length; i++) {
			if(nums[i] != nums[i-1]) {
				nums[index++] = nums[i];
			}
		}
		return index;
	}
}
