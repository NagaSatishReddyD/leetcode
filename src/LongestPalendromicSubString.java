
public class LongestPalendromicSubString {

	public static void main(String[] args) {
		System.out.println(longestPalindrome("babad"));
	}
	
	 public static String longestPalindrome(String s) {
	        if(isPalendrome(s)) {
	        	return s;
	        }
	        for(int i = 1; i < s.length()/2; i++) {
	        	boolean leaveLeftMost = isPalendrome(s.substring(i));
	        	boolean leaveRightMost = isPalendrome(s.substring(0, s.length() - i));
	        	if(leaveLeftMost) {
	        		return s.substring(i);
	        	}
	        	if(leaveRightMost) {
	        		return s.substring(0, s.length() - i);
	        	}
	        }
			return "";
	 }

	private static boolean isPalendrome(String s) {
		int i = 0;
		int j = s.length() - 1;
		while(i<=j) {
			if(s.charAt(i) == s.charAt(j)) {
				i++;
				j--;
			}else {
				return false;
			}
		}
		return true;
	}

}
